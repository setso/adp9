package main;


import adp9.Dijkstra;
import adp9.DykstraWrapper;
import adp9.ListGraph;
import adp9.ListPath;
import adp9.Vertex;

import java.util.Random;

public class Testbench {


    public static void main(String[] args) {

        System.out.println("Testbench started");
        int numberOfElements=1000;
        ListGraph listGraph = new ListGraph(numberOfElements);
        //MatrixGraph matrixGraph = new MatrixGraph();


        //Add vertexes
        for (int i =0; i < numberOfElements; i++){
            //Insert vertex with a random payload
            listGraph.insertVertex(new Vertex<String>(randomThreeCharString()));
        }
        System.out.println("Vertex 1 " + listGraph.getVertex(0).getData());
        System.out.println("Vertex 10 " + listGraph.getVertex(9).getData());

        //Add paths
        for (int i =0; i < numberOfElements; i++){
            System.out.println("Add a path");
            //Add a random number path of paths to the given vertex
            Vertex origin = listGraph.getVertex(i);

            int numberOfNeighbors = (int)(Math.random()*100)+1;
            System.out.println("Number of neighbors " + numberOfNeighbors);

            for (int n= 0; n < numberOfNeighbors; n++){
                int randNeighbor = (int)(Math.random()*numberOfElements);

                if (randNeighbor!=i){
                    Vertex neighbor = listGraph.getVertex(randNeighbor);
                    listGraph.addPath(origin, neighbor, (int)(Math.random()*15)+1);
                }
            }
        }

        
        Vertex target = listGraph.getVertex(7);
        
        DykstraWrapper dykstra = new DykstraWrapper(listGraph, target);
        dykstra.calculateDijkstraPathToTarget(target);
        
//        Dijkstra dijkstra = new Dijkstra (listGraph, target);
//        dijkstra.calculateDijkstraPathToTarget(target);
        
        
//        ListPath lp=listGraph.getVertex(1).getToTarget();
//        System.out.println("Weight to target " + lp.weight);
//
//        lp=listGraph.getVertex(5).getToTarget();
//        System.out.println("Weight to target " + lp.weight);
//
//        lp=listGraph.getVertex(7).getToTarget();
//        System.out.println("Weight to target " + lp.weight);

    }

    /**
     * Generates a string of three random chars
     * @return
     */
    private static String randomThreeCharString(){
        String ret="";
        Random r = new Random();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        for (int i = 0; i < 3; i++) {
            ret = ret + alphabet.charAt(r.nextInt(alphabet.length()));
        }
        return ret;
    }


}

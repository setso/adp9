package main;

import adp9.Dijkstra;
import adp9.GraphInterface;
import adp9.MatrixGraph;
import adp9.Vertex;

public class TestFrameMatrix {
	
	
	public static void main(String[] args) {
		
		MatrixGraph<Vertex> graph = new MatrixGraph<Vertex>(25);
		graph.addPath(new Vertex(1), new Vertex(2), 4);
		graph.addPath(new Vertex(1), new Vertex(7), 5);
		graph.addPath(new Vertex(1), new Vertex(6), 10);
		graph.addPath(new Vertex(2), new Vertex(7), 2);
		graph.addPath(new Vertex(2), new Vertex(3), 7);
        graph.addPath(new Vertex(7), new Vertex(6), 4);
        graph.addPath(new Vertex(7), new Vertex(3), 1);
        graph.addPath(new Vertex(7), new Vertex(5), 8);
        graph.addPath(new Vertex(6), new Vertex(5), 3);
        graph.addPath(new Vertex(3), new Vertex(4), 12);
        graph.addPath(new Vertex(5), new Vertex(4), 4);
        
       // graph.printAll();
		
		Dijkstra dyk = new Dijkstra<Integer>();
        dyk.doDykstraWithFixedStart(graph, new Integer(1));
        dyk.printDykstra();
        dyk.doDykstraWithFixedStart(graph, new Integer(4));
        dyk.printDykstra();
        

	

}
}

package adp9;

public class Vertex <T> {

	/**
	 * Containers Data
	 */
	private T data;


    /**
     * Stores the path to the target
     */
    private ListPath toTarget;

    /**
     * Flag for the state of the vertex.
     * Needed for Dykstra implementation
     */
    private boolean flag;

	/**
	 * Construktor
	 * @param data
	 */
	public Vertex(T data) {
		this.data = data;
	}
	
	/**
	 * Gibt die Daten des Containers zurueck.
	 * @return
	 */
	public T getData() {
		return data;
	}

    /**
     * Gets the current status
     *
     * @return true if flag is set
     */
    public boolean getFlag() {
        return flag;
    }

    /**
     * Sets the status
     *
     * @param _flag
     */
    public void setFlag(boolean _flag) {
        this.flag=_flag;
    }

    /**
     *
     * @return
     */
    public ListPath getToTarget() {
        return toTarget;
    }

    /**
     *
     * @param toTarget
     */
    public void setToTarget(ListPath toTarget) {
        this.toTarget=toTarget;
    }
    
    @Override
    public String toString()
    {
    	return String.format("[<Vertex> elem=%s]", data);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + (flag ? 1231 : 1237);
		result = prime * result
				+ ((toTarget == null) ? 0 : toTarget.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (flag != other.flag)
			return false;
		if (toTarget == null) {
			if (other.toTarget != null)
				return false;
		} else if (!toTarget.equals(other.toTarget))
			return false;
		return true;
	}
    
    
}

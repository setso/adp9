package adp9;



public class VertexWrapper<E> {
    private Vertex<E> ownVertex;
    private boolean marked;
    private VertexWrapper<E> related;
    private int costs;
    
    public VertexWrapper(Vertex<E> vertex) {
        ownVertex = vertex;
        marked = false;
        costs = (Integer.MAX_VALUE - 1);
        related = null;
    }

    public Vertex<E> getOwnVertex() {
        return ownVertex;
    }

    public boolean isMarked() {
        return marked;
    }

    public VertexWrapper<E> getRelated() {
        return related;
    }

    public int getCosts() {
        return costs;
    }

    public void setOwnVertex(Vertex<E> ownVertex) {
        this.ownVertex = ownVertex;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public void setRelated(VertexWrapper<E> related) {
        this.related = related;
    }

    public void setCosts(int costs) {
        this.costs = costs;
    }

    @Override
    public String toString() {
        return String.format("[<VW> e=%s, m=%s, r=%s, c=%s]", ownVertex.getData(), marked, related.getOwnVertex().getData(), costs);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ownVertex == null) ? 0 : ownVertex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        VertexWrapper other = (VertexWrapper) obj;
        if (ownVertex == null) {
            if (other.ownVertex != null) {
                return false;
            }
        } else if (!ownVertex.equals(other.ownVertex)) {
            return false;
        }
        return true;
    }
    
}

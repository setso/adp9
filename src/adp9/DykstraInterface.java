package adp9;


public interface DykstraInterface<E> {
	
    public abstract void doDykstraWithFixedStart(GraphInterface<E> graph, E start);

    public abstract void printDykstra();

}

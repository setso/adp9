package adp9;

/**
 * Created by bud on 29.11.15.
 */
public class ListPath {

    public Vertex neighbor;
    public int weight;

    public ListPath(Vertex _neighbor, int _weight){
        this.neighbor=_neighbor;
        this.weight=_weight;
    }
}

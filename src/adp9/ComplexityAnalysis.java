package adp9;

public class ComplexityAnalysis {

	public static void main(String[] args) {

		testMatrix();
		
	}

	private static void testMatrix() {
		MatrixGraph<Integer> graph;
		DykstraInterface<Integer> dyk;
		int n;
		
		// nur bis k^4, aber k^5 out of memory
		for(int i = 1; i <= 4; i++) {
			n = (int) Math.pow(10, i);
			graph = new MatrixGraph<Integer>(n);
			createGraph(graph, n);
			
			graph.printAll();
	        
	        dyk = new DykstraWrapper<Integer>();
	        dyk.doDykstraWithFixedStart(graph, new Integer(1));
	        dyk.printDykstra();
		}
	}
	

	
	private static void createGraph(GraphInterface<Integer> graph, int n) {	
		int[][] matrix = new int[n][n];
		
		for(int i = 0; i < n; i++){
		    graph.insertVertex(new Vertex(i));
		}
		
		for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                matrix[i][j] = (int) (Math.random()*2);
                matrix[j][i] = matrix[i][j];
                graph.addPath(new Vertex(i), new Vertex (j), matrix[i][j]);
            }
        }
	}
}

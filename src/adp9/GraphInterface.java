package adp9;

import java.util.List;

public interface GraphInterface<E> {
	

    /**

     * @param elem 
     * @return true if valid insert
     */
    boolean insertVertex(Vertex elem);


    /**
     *
     * @param v1
     * @param v2
     * @param weight
     * @return
     */
    boolean addPath(Vertex v1, Vertex v2, int weight);

    /**
     *
     * @param v1
     * @param v2
     * @return
     */
    boolean removePath(Vertex v1, Vertex v2);


    /**
     * Removes a Vertex and all his paths
     *
     * @param delVertex vertex to be removed
     * @return true if vertex removed
     */
    boolean removeVertex(Vertex delVertex);

    /**
     * Returns the neighbors of a given vertex
     *
     * @param vertex the vertex you want to get the neighbors from
     * @return
     */
    Vertex[] getNeighbors(Vertex vertex);

    /**
     * Returns an array with the weight of the path. The vertex is corresponding to the position in the array
     *
     * @param delVertex
     * @return
     */
    int[] getWeightToNeighbors(Vertex delVertex);


    /**
     * Returns the weight to the
     * calculatePathToTarget has to be excecuted before
     *
     * @param origin
     * @return
     */
    int getWeightToTarget(Vertex origin);
    
    public List<Vertex<E>> getNeighbors(E elem);
    
    public int getCost(E from, E to);
}

package adp9;

public class DykstraWrapper<E> extends Dijkstra<E>{
	
	
	
	
    public DykstraWrapper(ListGraph listGraph, Vertex target) {
        super(listGraph, target);
	}


	@Override
    public void doDykstraWithFixedStart(GraphInterface<E> graph, E start){
        
        System.out.println("StartTime");
        double before = System.nanoTime();
        super.doDykstraWithFixedStart(graph, start);
        double after = System.nanoTime();
        System.out.println("EndTime");
        
        double time = (after - before)/1000;
        System.out.println("Timediff in microSekunden: " + time);
        
        
    }
    
	@Override
    public void calculateDijkstraPathToTarget(Vertex target){
    	
        System.out.println("StartTime");
        double before = System.nanoTime();
        super.calculateDijkstraPathToTarget(target);
        double after = System.nanoTime();
        System.out.println("EndTime");
        
        double time = (after - before)/1000;
        System.out.println("Timediff in microSekunden: " + time);
        
        
    }
    
}

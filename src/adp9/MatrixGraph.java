package adp9;





import java.util.ArrayList;
import java.util.List;

public class MatrixGraph<E> implements GraphInterface<E> {

	int[][] matrix;
	List<Vertex<E>> vertexList = new ArrayList<Vertex<E>>();
	
	public MatrixGraph(int n) {
		matrix = new int[n][n];
	}
	@Override
	public boolean insertVertex(Vertex elem) {
        Vertex<E> vertex = new Vertex(elem);
        vertexList.add(vertex);
		return true;
	}

	public Vertex getVertex(int n)
	{
		return vertexList.get(n);
	}
	@Override
	public boolean addPath(Vertex from, Vertex to, int costs) {
		 Vertex<E> vertexFrom = new Vertex(from);
	        int indexFrom = vertexList.indexOf(vertexFrom);
	        if(indexFrom == -1){
	            vertexList.add(vertexFrom);
	            indexFrom = vertexList.indexOf(vertexFrom);
	        }
	        
	        Vertex<E> vertexTo = new Vertex(to);
	        int indexTo = vertexList.indexOf(vertexTo);
	        if(indexTo == -1){
	            vertexList.add(vertexTo);
	            indexTo = vertexList.indexOf(vertexTo);
	        }
	      
	        matrix[indexFrom][indexTo] = costs;
	        matrix[indexTo][indexFrom] = costs;      
		return true;
	}

	@Override
	public boolean removePath(Vertex v1, Vertex v2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeVertex(Vertex delVertex) {
        Vertex<E> vertex = new Vertex(delVertex);
        int index = vertexList.indexOf(vertex);
        vertex = vertexList.remove(index);
        
        for(int i = 0; i < matrix.length && index > -1; i++){
            matrix[index][i] = 0;
            matrix[i][index] = 0;
        }
		return true;
	}

	@Override
	public Vertex[] getNeighbors(Vertex vertex) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public List<Vertex<E>> getNeighbors(E vertex) {
		 List<Vertex<E>> neighbourList = new ArrayList<Vertex<E>>();
		 Vertex<E> vertexs = new Vertex<E>(vertex);
		 int index = vertexList.indexOf(vertexs);
		 
		 if(index != -1)
		 {
			 for(int i = 0; i <matrix[index].length; i++)
			 {
				 if(matrix[index][i] != 0)
				 {
					 neighbourList.add(vertexList.get(i));
				 }
			 }
		 }
		 return neighbourList;
	}

	@Override
	public int[] getWeightToNeighbors(Vertex delVertex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getWeightToTarget(Vertex origin) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public int getCost(E from, E to){
        Vertex<E> vertexFrom = new Vertex<E>(from);
        int indexFrom = vertexList.indexOf(vertexFrom);
        if(indexFrom == -1){
            return -1;
        }
        Vertex<E> vertexTo = new Vertex<E>(to);
        int indexTo = vertexList.indexOf(vertexTo);
        if(indexTo == -1){
            return -1;
        }
        
        int cost = matrix[indexFrom][indexTo];
        if(cost == 0){
            return -1;
        }
        return cost;
    }
	
	public void printAll() {
        System.out.println(vertexList.toString());
    }   
	
	

}

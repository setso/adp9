package adp9;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by bud on 29.11.15.
 */
public class ListGraph implements GraphInterface {


    Vertex[] vertexList;
    ArrayList<ListPath>[] neighborList;
    int size;
    ArrayList<Vertex> activeVertexes;

    /**
     * Constructor
     * @param _size maximum graph size
     */
    public ListGraph(int _size){
        vertexList=new Vertex[_size];
        neighborList= new ArrayList[_size];
        activeVertexes= new ArrayList();
        size=_size;
    }


    /**
     * Returns the postions of the given vertex. -1 if the vertex is not found
     *
     * @return int position of vertex
     */
    public int findVertex(Vertex vertex){
        for (int x = 0; x< size; x++){
            if (vertexList[x]==vertex) return x;
        }
        return -1;
    }

    @Override
    public boolean insertVertex(Vertex elem) {
        int s =0;
        //looks for the first free place in the list
        while(vertexList[s]!=null){
            s++;
        }
        vertexList[s]=elem;
        return false;
    }

    @Override
    public boolean addPath(Vertex v1, Vertex v2, int weight) {
        ListPath newPath=new ListPath(v2, weight);
        System.out.println();

        if (neighborList[findVertex(v1)]==null) neighborList[findVertex(v1)] =new ArrayList<ListPath>();
        neighborList[findVertex(v1)].add(newPath);
        newPath=new ListPath(v1, weight);
        if (neighborList[findVertex(v2)]==null) neighborList[findVertex(v2)] =new ArrayList<ListPath>();
        neighborList[findVertex(v2)].add(0, newPath);
        System.out.println("Added path " + v1.getData() + " " + v2.getData() + " weight " + weight);
        return true;
    }

    @Override
    public boolean removePath(Vertex v1, Vertex v2) {
        return false;
    }

    @Override
    public boolean removeVertex(Vertex delVertex) {
        return false;
    }

    /**
     * Returns an array with the neighbors for the given vertex
     * @param pos position of the vertex
     * @return Vertex[] neighbor vertexes
     */
    public Vertex[] getNeighbors(int pos) {
        int s =0;
        Vertex[] neighbors=new Vertex[neighborList[pos].size()];
        ArrayList<ListPath> pathArray = neighborList[pos];
        for (ListPath path : pathArray){
            neighbors[s]=path.neighbor;
            s++;
        }
        return neighbors;
    }

    @Override
    public Vertex[] getNeighbors(Vertex vertex) {
        int i = findVertex(vertex);
        return getNeighbors(i);
    }

    /**
     * Returns an array with the weight to the neighbors
     *
     * @param pos
     * @return int[]
     */
    public int[] getWeightToNeighbors(int pos){
        int s =0;
        int[] neighborsWeight=new int[neighborList[pos].size()];
        ArrayList<ListPath> pathArray = neighborList[pos];
        for (ListPath path : pathArray){
            neighborsWeight[s]=path.weight;
            s++;
        }
        return neighborsWeight;
    }

    @Override
    public int[] getWeightToNeighbors(Vertex vertex) {
        int i = findVertex(vertex);
        return getWeightToNeighbors(i);
    }



    @Override
    public int getWeightToTarget(Vertex origin) {
        return 0;
    }

    /**
     *
     * @param i
     * @return
     */
    public Vertex getVertex(int i) {
        return vertexList[i];
    }


	@Override
	public List getNeighbors(Object elem) {
		// NOT IMPORTANT FOR ME
		return null;
	}


	@Override
	public int getCost(Object from, Object to) {
		// TODO Auto-generated method stub
		return 0;
	}


}

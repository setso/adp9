package adp9;


import java.util.ArrayList;
import java.util.List;

public class Dijkstra<E> implements DykstraInterface<E> {
	
    ArrayList<Vertex> activeVertexes;
    GraphInterface graph;
    Vertex target;
    
    
    E start = null;
    List<VertexWrapper<E>> markedList;
    
	public Dijkstra(GraphInterface _graph, Vertex _target){
		graph = _graph;
		target = _target;
		activeVertexes= new ArrayList();
		
	}
	
	public Dijkstra() {
		// TODO Auto-generated constructor stub
	}
	
	public void calculateDijkstraPathToTarget(Vertex _target) {
		
        ListPath listPath = new ListPath(null, 0);
        target.setToTarget(listPath);
        target.setFlag(true);

        activeVertexes.add(target);
        calculatePathToTarget(target, target);
    }
	

    /**
     *
     * @param target
     * @param currentVertex
     */
    private void calculatePathToTarget(Vertex target, Vertex currentVertex){
        Vertex[] neighbors = graph.getNeighbors(currentVertex);
        int[] neighborsWeight =  graph.getWeightToNeighbors(currentVertex);
        int s=0;

        for (Vertex neighbor:neighbors){
            if (!activeVertexes.contains(neighbor)){
                activeVertexes.add(neighbor);
            }
            ListPath newPath = new ListPath(currentVertex, neighborsWeight[s]+currentVertex.getToTarget().weight);
            ListPath oldPath=neighbor.getToTarget();
            if (oldPath==null){
                neighbor.setToTarget(newPath);
            }
            else if (newPath.weight<oldPath.weight){
                neighbor.setToTarget(newPath);
            }
            s++;
        }
        Vertex nextVertex = getSmallestUnmarkedWeightVertex();

        if (nextVertex!=null){
            nextVertex.setFlag(true);
            calculatePathToTarget(target, nextVertex);
        }

    }
    
    /**
    *
    * @return
    */
   private Vertex getSmallestUnmarkedWeightVertex(){
       Vertex found = null;

       for (int i =0; i< activeVertexes.size(); i++){
           if (found==null&&activeVertexes.get(i).getFlag()==false){
               found=activeVertexes.get(i);
           }
           else if (activeVertexes.get(i).getFlag()==false&&found.getToTarget().weight>activeVertexes.get(i).getToTarget().weight){
               found=activeVertexes.get(i);
           }
       }

       return found;
   }
   
   
   
   
   ///*******************************************************************************************************************//
   
   @Override
   public void doDykstraWithFixedStart(GraphInterface<E> graph, E start){
       this.graph = graph;
       this.start = start;
       markedList = new ArrayList<VertexWrapper<E>>();
       List<VertexWrapper<E>> notMarkedList = new ArrayList<VertexWrapper<E>>();
       
       VertexWrapper<E> nextVertexWrapper = new VertexWrapper<E>(new Vertex<E>(start));
       nextVertexWrapper.setCosts(0);
       nextVertexWrapper.setMarked(true);
       nextVertexWrapper.setRelated(nextVertexWrapper);
       markedList.add(nextVertexWrapper);
       
       E next = start;
       VertexWrapper<E> vW;
       List<Vertex<E>> neighnourList = graph.getNeighbors(next);
       int costs;
       int indexLowestCosts;
       int lowestCosts;
       
       do{
           
           for(Vertex<E> elem: neighnourList){
               if(elem != null){
                   vW = new VertexWrapper<E>(elem);
                   if(markedList.indexOf(vW) == -1 && notMarkedList.indexOf(vW) == -1){
                       notMarkedList.add(vW);
                   } 
               }   
           }
           
           for(VertexWrapper<E> elem: notMarkedList){
               E to = elem.getOwnVertex().getData();
               costs = graph.getCost(nextVertexWrapper.getOwnVertex().getData(), to);
               
               if(costs > -1){
                   costs += nextVertexWrapper.getCosts();
                   if(costs < elem.getCosts()){
                       elem.setCosts(costs);
                       elem.setRelated(nextVertexWrapper);
                   }  
               }    
           }
           
           lowestCosts = Integer.MAX_VALUE;
           indexLowestCosts = -1;
           for(int i = 0; i < notMarkedList.size(); i++){
               costs = notMarkedList.get(i).getCosts();
               if (costs < lowestCosts){
                   indexLowestCosts = i;
                   lowestCosts = costs;
               }
           }
           
           if(indexLowestCosts > -1){
               nextVertexWrapper = notMarkedList.remove(indexLowestCosts);
               nextVertexWrapper.setMarked(true);
               markedList.add(nextVertexWrapper);
               neighnourList = graph.getNeighbors(nextVertexWrapper.getOwnVertex().getData());
           }
             
       } while(notMarkedList.size() > 0); 
       
   }
   
   @Override
   public void printDykstra(){
       StringBuilder sB = new StringBuilder();
       
       sB.append("[StartElement: ");
       sB.append(start);
       sB.append(" Dykstra:( ");
       for(int i = 0; i < markedList.size(); i++){
           sB.append(markedList.get(i));
           if((i+1) < markedList.size()){
               sB.append(", ");
           }
       }
       sB.append(")]");
       System.out.println(sB.toString());
   }
    
}
